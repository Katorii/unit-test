// test/example.test.js

const expect = require('chai').expect;
const should = require('chai').should();
const { assert } = require('chai');
const mylib = require('../src/mylib');

// Unit testing
describe('Unit testing mylib.js', () => {

    let myvar = undefined;

    // Testing before
    before(() => {
        myvar = 1;                          // setup before testing
        console.log('Before testing');
    });

    // Testing expect
    it('Should return 2 when using sum function with a=1, b=1', () => {
        const result = mylib.sum(1, 1);     // 1 + 1
        expect(result).to.equal(2);         // result expected to equal 2
    });

    // Testing expect with different function
    it('Should return random number between 0-1', () => {
        const result = mylib.random();      // Random number between 0-1
        
        expect(result).to.lessThan(2);      // Be less than 2
        expect(result).to.greaterThan(-1);  // Be greater than -1
    });

    // Testing parametrized
    it('Parametrized way of unit testing', () => {
        const result = mylib.sum(myvar, myvar);
        expect(result).to.equal(myvar + myvar);
    });

    // Testing assert
    it('Assert foo is not bar', () => {
        assert('foo' !== 'bar');            // true
    });

    // Testing should
    it('Myvar should exist', () => {
        should.exist(myvar);
    });

    // Testing timeout
    it('Should take less than 500ms', function(done) {
        this.timeout(500);
        setTimeout(done, 300);
    });

    // Testing skip
    it.skip('Skip this one', () => {
        should.exist(myvar);
    });

    // Testing after
    after(() => {
        console.log('After testing');
    });

    // Testing afterEach
    afterEach(() => {
        console.log('-------------------------');
    });
})